// Arrays

let registeredUsers = [

	"jamieJamie21",
	"45_gunther",
	"KingOfTheWest_2003",
	"freedomFighter47",
	"kennethTheKnight",
	"theScepter07",
	"ninjaisme00"

];

let friendsList = [];


// FUNCTION 1 
// register user - receive string and user should not duplicate

function registerUser(username){

	doesUserExist = registeredUsers.includes(username);
	if(doesUserExist){
		alert("Registration failed. Username already exists!");
	} 
	else {
		registeredUsers.push(username);
		alert("Thank you for registering!");
	}

};

// FUNCTION 2
// add friend - receive string and add to friendsList from existing usernames

function addFriend(username){

	foundUser = registeredUsers.find(function(user){
		return username === user;

	});

	if(foundUser){
		friendsList.push(username);
		alert(`You have added ${username} as a friend!`);
	}
	else {
		alert("User not found.");
	}
};

// FUNCTION 3
// display each friend - display items in friendsList one by one on console.

function displayFriends(){

	if(friendsList.length>0){
		friendsList.forEach(function(user){
		console.log(user);
		})
	}
	else {
		alert("You currently have 0 friends. Add one first.");
	}
};

// FUNCTION 4
// display amount of users in friendsList

function countFriends(){

	if(friendsList.length>0){
		alert(`You currently have ${friendsList.length} friends.`);
	}else{
		alert("You currently have 0 friends. Add one first.");
	}
}

// FUNCTION 5
// delete specific user from friendsList

function deleteFriend(username){

	if (friendsList.length>0) {

		userIndex = friendsList.indexOf(username);
		friendsList.splice(userIndex,1);

	}
	else {
		alert("You currently have 0 friends. Add one first.")
	}
};